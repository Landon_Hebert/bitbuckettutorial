﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

//Created by Landon Hebert
//Created November 2019
//This program is a playable version of Tic Tac Toe on a Windows form

namespace LHebertAssignment3
{
    public partial class frmGameboard : Form
    {
        bool xPlayerTurn;
        string[,] tiles = new string[3,3];
        const string X_STRING = "x";
        const string O_STRING = "o";

        public frmGameboard()
        {
            InitializeComponent();
        }

        private void frmGameboard_Load(object sender, EventArgs e)
        {
            //initialze the gameboard
            GameStart();
        }

        /// <summary>
        /// Sets initial state of gameboard and resets the game if filled
        /// </summary>
        public void GameStart()
        {
            xPlayerTurn = true;
            lblPlayerTurn.Text = "X Player's Turn";
            lblLastTurn.Text = "";
            foreach (PictureBox pictureBox in pnlTicTacToeBoard.Controls)
            {
                //reset tile picture
                pictureBox.Image = null;
            }
            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                for (int j = 0; j < tiles.GetLength(1); j++)
                {
                    //reset tile state
                    tiles[i, j] = "";
                }
            }
        }

        private void picA1_Click(object sender, EventArgs e)
        {
            tileClick(picA1, 0, 0);
        }

        private void picA2_Click(object sender, EventArgs e)
        {
            tileClick(picA2, 1, 0);
        }

        private void picA3_Click(object sender, EventArgs e)
        {
            tileClick(picA3, 2, 0);
        }

        private void picB1_Click(object sender, EventArgs e)
        {
            tileClick(picB1, 0, 1);
        }

        private void picB2_Click(object sender, EventArgs e)
        {
            tileClick(picB2, 1, 1);
        }

        private void picB3_Click(object sender, EventArgs e)
        {
            tileClick(picB3, 2, 1);
        }

        private void picC1_Click(object sender, EventArgs e)
        {
            tileClick(picC1, 0, 2);
        }

        private void picC2_Click(object sender, EventArgs e)
        {
            tileClick(picC2, 1, 2);
        }

        private void picC3_Click(object sender, EventArgs e)
        {
            tileClick(picC3, 2, 2);
        }

        /// <summary>
        /// Change image of tile and check for winner
        /// </summary>
        /// <param name="gameTile"></param>
        /// <param name="xLocation"></param>
        /// <param name="yLocation"></param>
        public void tileClick(PictureBox gameTile, int xLocation, int yLocation)
        {
            bool gameReset = false;
            //check if valid tile was selected
            bool succeed = ChangeImage(gameTile, xPlayerTurn, xLocation, yLocation);
            //check for x winner
            if (xPlayerTurn && IsWinner(X_STRING))
            {
                gameReset = true;
                MessageBox.Show("X player wins");
            }
            else if (IsWinner(O_STRING))
            {
                gameReset = true;
                MessageBox.Show("O player wins");
            }
            else if(IsTie())
            {
                gameReset = true;
                MessageBox.Show("The game is a tie. No winner.");
            }
            if (gameReset)
            {
                //reset game
                GameStart();
            }
            //go to next player if valid tile was selected
            else if (succeed)
            {
                xPlayerTurn = !xPlayerTurn;
            }
        }

        /// <summary>
        /// Change image of tile to player's corresponding symbol
        /// </summary>
        /// <param name="gameTile"></param>
        /// <param name="xPlayerTurn"></param>
        /// <param name="xLocation"></param>
        /// <param name="yLocation"></param>
        /// <returns></returns>
        public bool ChangeImage(PictureBox gameTile, bool xPlayerTurn, int xLocation, int yLocation)
        {
            if (gameTile.Image == null)
            {
                if (xPlayerTurn)
                {
                    gameTile.Image = Properties.Resources.xImage;
                    tiles[xLocation, yLocation] = "x";
                    lblPlayerTurn.Text = "O Player's Turn";
                    lblLastTurn.Text = $"X added to tile {xLocation+1}, {yLocation+1}";
                }
                else
                {
                    gameTile.Image = Properties.Resources.oImage;
                    tiles[xLocation, yLocation] = "o";
                    lblPlayerTurn.Text = "X Player's Turn";
                    lblLastTurn.Text = $"O added to tile {xLocation + 1}, {yLocation + 1}";
                }
                //return true if change succeeded
                return true;
            }
            else
            {
                //if selected tile already has an image, return failure
                MessageBox.Show("Please select an empty tile.");
                return false;
            }
        }

        /// <summary>
        /// Check for game tie
        /// </summary>
        /// <returns></returns>
        public bool IsTie()
        {
            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                for (int j = 0; j < tiles.GetLength(0); j++)
                {
                    //check if a tile is still open
                    if (tiles[i, j] == "")
                        return false;
                }
            }
            //returns true only if all tiles are filled
            return true;
        }

        /// <summary>
        /// Checks for three of the same letter in a row
        /// </summary>
        /// <param name="checkLetter"></param>
        /// <returns></returns>
        public bool IsWinner(string checkLetter)
        {
            //check the horizontal and vertical three-in-a-row possibilites
            for (int i = 0; i < tiles.GetLength(0); i++)
            {
                int horizontalCheck = 0;
                int verticalCheck = 0;
                for (int j = 0; j < tiles.GetLength(0); j++)
                {
                    if(tiles[i,j]==checkLetter)
                    {
                        verticalCheck++;
                    }
                    if (tiles[j, i] == checkLetter)
                    {
                        horizontalCheck++;
                    }
                }
                if (verticalCheck == 3 || horizontalCheck == 3)
                    return true;
            }
            //check for diagonal three-in-a-row possibilites
            if((tiles[0,0]==checkLetter&& tiles[1, 1] == checkLetter && tiles[2, 2] == checkLetter)||(tiles[0, 2] == checkLetter && tiles[1, 1] == checkLetter && tiles[2, 0] == checkLetter))
            {
                return true;
            }
            //return false if none of the checks pass
            return false;
        }
    }
}
