﻿namespace LHebertAssignment3
{
    partial class frmGameboard
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.picC3 = new System.Windows.Forms.PictureBox();
            this.picC2 = new System.Windows.Forms.PictureBox();
            this.picC1 = new System.Windows.Forms.PictureBox();
            this.picB3 = new System.Windows.Forms.PictureBox();
            this.picB2 = new System.Windows.Forms.PictureBox();
            this.picB1 = new System.Windows.Forms.PictureBox();
            this.picA3 = new System.Windows.Forms.PictureBox();
            this.picA2 = new System.Windows.Forms.PictureBox();
            this.picA1 = new System.Windows.Forms.PictureBox();
            this.pnlTicTacToeBoard = new System.Windows.Forms.Panel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblPlayerTurn = new System.Windows.Forms.Label();
            this.lblLastTurn = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picC3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picC2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picC1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picA3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picA2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picA1)).BeginInit();
            this.pnlTicTacToeBoard.SuspendLayout();
            this.SuspendLayout();
            // 
            // picC3
            // 
            this.picC3.BackColor = System.Drawing.Color.DarkOrange;
            this.picC3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picC3.Location = new System.Drawing.Point(215, 215);
            this.picC3.Name = "picC3";
            this.picC3.Size = new System.Drawing.Size(100, 100);
            this.picC3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picC3.TabIndex = 8;
            this.picC3.TabStop = false;
            this.picC3.Click += new System.EventHandler(this.picC3_Click);
            // 
            // picC2
            // 
            this.picC2.BackColor = System.Drawing.Color.DarkOrange;
            this.picC2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picC2.Location = new System.Drawing.Point(109, 215);
            this.picC2.Name = "picC2";
            this.picC2.Size = new System.Drawing.Size(100, 100);
            this.picC2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picC2.TabIndex = 7;
            this.picC2.TabStop = false;
            this.picC2.Click += new System.EventHandler(this.picC2_Click);
            // 
            // picC1
            // 
            this.picC1.BackColor = System.Drawing.Color.DarkOrange;
            this.picC1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picC1.Location = new System.Drawing.Point(3, 215);
            this.picC1.Name = "picC1";
            this.picC1.Size = new System.Drawing.Size(100, 100);
            this.picC1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picC1.TabIndex = 6;
            this.picC1.TabStop = false;
            this.picC1.Click += new System.EventHandler(this.picC1_Click);
            // 
            // picB3
            // 
            this.picB3.BackColor = System.Drawing.Color.DarkOrange;
            this.picB3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picB3.Location = new System.Drawing.Point(215, 109);
            this.picB3.Name = "picB3";
            this.picB3.Size = new System.Drawing.Size(100, 100);
            this.picB3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picB3.TabIndex = 5;
            this.picB3.TabStop = false;
            this.picB3.Click += new System.EventHandler(this.picB3_Click);
            // 
            // picB2
            // 
            this.picB2.BackColor = System.Drawing.Color.DarkOrange;
            this.picB2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picB2.Location = new System.Drawing.Point(109, 109);
            this.picB2.Name = "picB2";
            this.picB2.Size = new System.Drawing.Size(100, 100);
            this.picB2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picB2.TabIndex = 4;
            this.picB2.TabStop = false;
            this.picB2.Click += new System.EventHandler(this.picB2_Click);
            // 
            // picB1
            // 
            this.picB1.BackColor = System.Drawing.Color.DarkOrange;
            this.picB1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picB1.Location = new System.Drawing.Point(3, 109);
            this.picB1.Name = "picB1";
            this.picB1.Size = new System.Drawing.Size(100, 100);
            this.picB1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picB1.TabIndex = 3;
            this.picB1.TabStop = false;
            this.picB1.Click += new System.EventHandler(this.picB1_Click);
            // 
            // picA3
            // 
            this.picA3.BackColor = System.Drawing.Color.DarkOrange;
            this.picA3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picA3.Location = new System.Drawing.Point(215, 3);
            this.picA3.Name = "picA3";
            this.picA3.Size = new System.Drawing.Size(100, 100);
            this.picA3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picA3.TabIndex = 2;
            this.picA3.TabStop = false;
            this.picA3.Click += new System.EventHandler(this.picA3_Click);
            // 
            // picA2
            // 
            this.picA2.BackColor = System.Drawing.Color.DarkOrange;
            this.picA2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picA2.Location = new System.Drawing.Point(109, 3);
            this.picA2.Name = "picA2";
            this.picA2.Size = new System.Drawing.Size(100, 100);
            this.picA2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picA2.TabIndex = 1;
            this.picA2.TabStop = false;
            this.picA2.Click += new System.EventHandler(this.picA2_Click);
            // 
            // picA1
            // 
            this.picA1.BackColor = System.Drawing.Color.DarkOrange;
            this.picA1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picA1.Location = new System.Drawing.Point(3, 3);
            this.picA1.Name = "picA1";
            this.picA1.Size = new System.Drawing.Size(100, 100);
            this.picA1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.picA1.TabIndex = 0;
            this.picA1.TabStop = false;
            this.picA1.Click += new System.EventHandler(this.picA1_Click);
            // 
            // pnlTicTacToeBoard
            // 
            this.pnlTicTacToeBoard.Controls.Add(this.picC3);
            this.pnlTicTacToeBoard.Controls.Add(this.picA1);
            this.pnlTicTacToeBoard.Controls.Add(this.picC2);
            this.pnlTicTacToeBoard.Controls.Add(this.picA2);
            this.pnlTicTacToeBoard.Controls.Add(this.picC1);
            this.pnlTicTacToeBoard.Controls.Add(this.picA3);
            this.pnlTicTacToeBoard.Controls.Add(this.picB3);
            this.pnlTicTacToeBoard.Controls.Add(this.picB1);
            this.pnlTicTacToeBoard.Controls.Add(this.picB2);
            this.pnlTicTacToeBoard.Location = new System.Drawing.Point(82, 125);
            this.pnlTicTacToeBoard.Name = "pnlTicTacToeBoard";
            this.pnlTicTacToeBoard.Size = new System.Drawing.Size(321, 319);
            this.pnlTicTacToeBoard.TabIndex = 9;
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Font = new System.Drawing.Font("Impact", 24F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.SystemColors.Control;
            this.lblTitle.Location = new System.Drawing.Point(64, 44);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(361, 48);
            this.lblTitle.TabIndex = 10;
            this.lblTitle.Text = "EXTREME Tic Tac Toe";
            // 
            // lblPlayerTurn
            // 
            this.lblPlayerTurn.AutoSize = true;
            this.lblPlayerTurn.Font = new System.Drawing.Font("Impact", 16.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPlayerTurn.ForeColor = System.Drawing.SystemColors.Control;
            this.lblPlayerTurn.Location = new System.Drawing.Point(412, 154);
            this.lblPlayerTurn.Name = "lblPlayerTurn";
            this.lblPlayerTurn.Size = new System.Drawing.Size(0, 35);
            this.lblPlayerTurn.TabIndex = 11;
            // 
            // lblLastTurn
            // 
            this.lblLastTurn.Font = new System.Drawing.Font("Impact", 16.2F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLastTurn.ForeColor = System.Drawing.SystemColors.Control;
            this.lblLastTurn.Location = new System.Drawing.Point(412, 218);
            this.lblLastTurn.Name = "lblLastTurn";
            this.lblLastTurn.Size = new System.Drawing.Size(144, 83);
            this.lblLastTurn.TabIndex = 12;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Impact", 10F, System.Drawing.FontStyle.Italic);
            this.lblName.ForeColor = System.Drawing.SystemColors.Control;
            this.lblName.Location = new System.Drawing.Point(12, 463);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(217, 21);
            this.lblName.TabIndex = 13;
            this.lblName.Text = "Game made by Landon Hebert";
            // 
            // frmGameboard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Firebrick;
            this.ClientSize = new System.Drawing.Size(665, 493);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblLastTurn);
            this.Controls.Add(this.lblPlayerTurn);
            this.Controls.Add(this.lblTitle);
            this.Controls.Add(this.pnlTicTacToeBoard);
            this.Name = "frmGameboard";
            this.Text = "Tic Tac Toe";
            this.Load += new System.EventHandler(this.frmGameboard_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picC3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picC2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picC1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picB1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picA3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picA2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picA1)).EndInit();
            this.pnlTicTacToeBoard.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picA1;
        private System.Windows.Forms.PictureBox picA2;
        private System.Windows.Forms.PictureBox picA3;
        private System.Windows.Forms.PictureBox picB3;
        private System.Windows.Forms.PictureBox picB2;
        private System.Windows.Forms.PictureBox picB1;
        private System.Windows.Forms.PictureBox picC3;
        private System.Windows.Forms.PictureBox picC2;
        private System.Windows.Forms.PictureBox picC1;
        private System.Windows.Forms.Panel pnlTicTacToeBoard;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblPlayerTurn;
        private System.Windows.Forms.Label lblLastTurn;
        private System.Windows.Forms.Label lblName;
    }
}

